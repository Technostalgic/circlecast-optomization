/** @type {HTMLCanvasElement} */
let canvas = null;
/** @type {CanvasRenderingContext2D} */
let ctx = null;
/** @type {Circle} */
let circ = null;
/** @type {Rect} */
let rec = null;
/** @type {Vec2} */
let raycastStart = null;
/** @type {Vec2} */
let raycastEnd = null;
/** @type {Vec2} */
let circlecastStart = null;
/** @type {Vec2} */
let circlecastEnd = null;
/** @type {RaycastResult} */
let rayHitCirc = null;
/** @type {RaycastResult} */
let rayHitRect = null;
/** @type {CirclecastResult} */
let circHitCirc = null;
/** @type {CirclecastResult_rect} */
let circHitRect = null;
/** @type {Number} */
let circlecastWidth = 10;

// clear the canvas to a blank color
function clearCanvas(){
	let canvas = document.getElementById("rCanvas");
	let ctx = canvas.getContext("2d");
	ctx.fillStyle = "#111";
	ctx.fillRect(0, 0, canvas.width, canvas.height);
}

// initialize the global variables
function initializeObjects(){

	canvas = document.getElementById("rCanvas");
	ctx = canvas.getContext("2d");
	
	let pos = new Vec2(canvas.width / 2, canvas.height / 2);
	circ = new Circle(pos, canvas.width / 3);
	rec = new Rect(new Vec2(), new Vec2(50, 30));
	rec.center = new Vec2(canvas.width / 2, canvas.height / 2);
	
	circlecastStart = new Vec2(circlecastWidth + 1);
	circlecastEnd = new Vec2(canvas.width / 2, canvas.height / 2 + 10);

	raycastStart = new Vec2(1);
	raycastEnd = new Vec2(canvas.width / 2, canvas.height / 2);
}

// draw the objects defined in the global scope
function drawObjects(){
	circ.drawOutline(ctx, "#FFF");
	rec.drawOutline(ctx, "FFF");
	drawCirclecast();
	//drawRaycast();
}

/**
 * draws a line from point a to b
 * @param {Vec2} pointA the start of the line
 * @param {Vec2} pointB the end of the line
 * @param {CanvasRenderingContext2D} ctx the canvas context to render with
 * @param {String} strokeStyle the stroke style to render with
 * @param {Number} lineWidth
 */
function drawLine(ctx, pointA, pointB, strokeStyle, lineWidth = 1){
	ctx.lineWidth = lineWidth;
	ctx.strokeStyle = strokeStyle;

	ctx.beginPath();
	ctx.moveTo(pointA.x, pointA.y);
	ctx.lineTo(pointB.x, pointB.y);
	ctx.stroke();
}

// draw the circlecast and it's collision points / normals
function drawCirclecast(){
	
	let ray = new Ray(circlecastStart, circlecastEnd);
	let tArcBegin = circlecastEnd.plus(Vec2.FromDirection(ray.dif.direction - Math.PI / 2, circlecastWidth));
	
	ctx.strokeStyle = "#6B6";
	ctx.beginPath();
	ctx.arc(
		ray.rayStart.x, ray.rayStart.y, circlecastWidth, 
		ray.dif.direction + Math.PI / 2, ray.dif.direction + 3 * Math.PI / 2);
	
	ctx.lineTo(tArcBegin.x, tArcBegin.y);
	ctx.arc(
		ray.rayEnd.x, ray.rayEnd.y, circlecastWidth, 
		ray.dif.direction - Math.PI / 2, ray.dif.direction + Math.PI / 2);

	ctx.closePath();
	ctx.stroke();
	
	circHitCirc = circ.circleCast(ray, circlecastWidth);
	if(circHitCirc.isValid){
		if(circHitCirc.didEnter){
			circHitCirc.entryPoint.draw(ctx, "#00F", 3);
			ctx.strokeStyle = "#aaa";
			ctx.beginPath();
			ctx.arc(circHitCirc.entryPoint.x, circHitCirc.entryPoint.y, circlecastWidth, 0, Math.PI * 2);
			ctx.stroke();
			circHitCirc.firstContact.draw(ctx, "#00F", 5);
			for(let i = 10; i >= 1; i--){
				let d = i * 5;
				let np = circHitCirc.firstContact.plus(circHitCirc.entryNormal.scale(d));
				np.draw(ctx, "#AAF", 2)
			}
		}
		if(circHitCirc.didExit){
			circHitCirc.exitPoint.draw(ctx, "#0AF", 3);
			circHitCirc.lastContact.draw(ctx, "#0AF", 5);
			ctx.strokeStyle = "#aaa";
			ctx.beginPath();
			ctx.arc(circHitCirc.exitPoint.x, circHitCirc.exitPoint.y, circlecastWidth, 0, Math.PI * 2);
			ctx.stroke();
			for(let i = 10; i >= 1; i--){
				let d = i * 5;
				let np = circHitCirc.lastContact.plus(circHitCirc.exitNormal.scale(d));
				np.draw(ctx, "#ACF", 2)
			}
		}
	}

	circHitRect = rec.circleCast(ray, circlecastWidth);
	if(circHitRect.isValid){
		if(circHitRect.didEnter){
			circHitRect.entryPoint.draw(ctx, "#00F", 3);
			ctx.strokeStyle = "#aaa";
			ctx.beginPath();
			ctx.arc(circHitRect.entryPoint.x, circHitRect.entryPoint.y, circlecastWidth, 0, Math.PI * 2);
			ctx.stroke();
			circHitRect.firstContact.draw(ctx, "#00F", 5);
			for(let i = 10; i >= 1; i--){
				let d = i * 5;
				let np = circHitRect.firstContact.plus(circHitRect.entryNormal.scale(d));
				np.draw(ctx, "#AAF", 2)
			}
		}
		if(circHitRect.didExit){
			circHitRect.exitPoint.draw(ctx, "#0AF", 3);
			circHitRect.lastContact.draw(ctx, "#0AF", 5);
			ctx.strokeStyle = "#aaa";
			ctx.beginPath();
			ctx.arc(circHitRect.exitPoint.x, circHitRect.exitPoint.y, circlecastWidth, 0, Math.PI * 2);
			ctx.stroke();
			for(let i = 10; i >= 1; i--){
				let d = i * 5;
				let np = circHitRect.lastContact.plus(circHitRect.exitNormal.scale(d));
				np.draw(ctx, "#ACF", 2)
			}
		}
	}
}

// draw the raycast and it's collision points / normals
function drawRaycast(){
	
	let dif = raycastEnd.minus(raycastStart);
	let tArcBegin = raycastEnd.plus(Vec2.FromDirection(dif.direction - Math.PI / 2, circlecastWidth));
	drawLine(ctx, raycastStart, raycastEnd, "#6B6");
	
	rayHitCirc = circ.rayCast(new Ray(raycastStart, raycastEnd));
	if(rayHitCirc.isValid){
		if(rayHitCirc.didEnter){
			rayHitCirc.entryPoint.draw(ctx, "#00F", 3);
			for(let i = 10; i >= 1; i--){
				let d = i * 5;
				let np = rayHitCirc.entryPoint.plus(rayHitCirc.entryNormal.scale(d));
				np.draw(ctx, "#AAF", 2)
			}
		}
		if(rayHitCirc.didExit){
			rayHitCirc.exitPoint.draw(ctx, "#0AF", 3);
			for(let i = 10; i >= 1; i--){
				let d = i * 5;
				let np = rayHitCirc.exitPoint.plus(rayHitCirc.exitNormal.scale(d));
				np.draw(ctx, "#ACF", 2)
			}
		}
	}

	rayHitRect = rec.rayCast(new Ray(raycastStart, raycastEnd));
	if(rayHitRect.isValid){
		if(rayHitRect.didEnter){
			rayHitRect.entryPoint.draw(ctx, "#00F", 3);
			for(let i = 10; i >= 1; i--){
				let d = i * 5;
				let np = rayHitRect.entryPoint.plus(rayHitRect.entryNormal.scale(d));
				np.draw(ctx, "#AAF", 2)
			}
		}
		if(rayHitRect.didExit){
			rayHitRect.exitPoint.draw(ctx, "#0AF", 3);
			for(let i = 10; i >= 1; i--){
				let d = i * 5;
				let np = rayHitRect.exitPoint.plus(rayHitRect.exitNormal.scale(d));
				np.draw(ctx, "#ACF", 2)
			}
		}
	}
}

// callback for when user moves the mouse
function onMouseMove(e){
	raycastEnd = new Vec2(e.offsetX, e.offsetY);
	circlecastEnd = new Vec2(e.offsetX, e.offsetY);
	clearCanvas();
	drawObjects();
}
// callback for when user clicks on the canvas
function onMouseDown(e){
	raycastStart = new Vec2(e.offsetX, e.offsetY);
	circlecastStart = new Vec2(e.offsetX, e.offsetY);
	clearCanvas();
	drawObjects();
}

// initialize the script when the browser finishes doing its thang
window.onload = function(){
	console.log("Start");
	initializeObjects();
	clearCanvas();
	drawObjects();
	
	canvas.addEventListener("mousemove", onMouseMove);
	canvas.addEventListener("mousedown", onMouseDown);
};